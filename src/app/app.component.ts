import {Component, OnInit} from '@angular/core';
import { ClimaService} from './services/clima.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'clima';
  temperatura: number = 0;
  tiempoBogota: string = '';
  iconoBogota: string = '';

  temperaturaLyon: number = 0;
  humedadLyon: number = 0;
  vientoLyon: number = 0;
  iconoLyon: string = '';

  temperaturaParis: number = 0;
  humedadParis: number = 0;
  vientoParis: number = 0;
  iconoParis: string = '';

  hoyMinima: number = 0;
  hoyMaxima: number = 0;

  manianaMinima: number = 0;
  manianaMaxima: number = 0;

  pasadoMinima: number = 0;
  pasadoMaxima: number = 0;

  hoyTiempo: string = '';
  manianaTiempo: string = '';
  pasadoTiempo: string = '';

  hoyIcono: string = '';
  manianaIcono: string = '';
  pasadoIcono: string = '';




  dias = ['Domingo', 'Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado' ];
  hoy = this.dias[new Date().getDay()];
  maniana = this.dias[new Date().getDay() + 1];
  pasadoManiana = this.dias[new Date().getDay() + 2];

  constructor( private climaService: ClimaService) {

  }
  ngOnInit() {
    this.showWeatherBogota();
    this.showWeatherPredictionsBogota();
    this.showWeatherLyon();
    this.showWeatherParis();
  }

  showWeatherBogota(){
    this.climaService.getWeatherBogota().subscribe(
      data =>{
        this.temperatura = Math.round(data['main']['temp']);
        this.tiempoBogota = data['weather']['description'];
        if(data['weather']['main'] == 'Clouds'){
          this.iconoBogota = 'nubes.png';
        }
        else if(data['weather']['main'] == 'Rain'){
          this.iconoBogota = 'rain.png';
        }
        else{
          this.iconoBogota = 'dom.png';
        }
      }
    )
  }

  showWeatherLyon(){
    this.climaService.getWeatherLyon().subscribe(
      data =>{
        this.temperaturaLyon = Math.round(data['main']['temp']);
        this.humedadLyon = data['main']['humidity'];
        this.vientoLyon = data['wind']['speed'];
        if(data['weather']['main'] == 'Clouds'){
          this.iconoLyon = 'nubes.png';
        }
        else if(data['weather']['main'] == 'Rain'){
          this.iconoLyon = 'rain.png';
        }
        else{
          this.iconoLyon = 'dom.png';
        }
      }
    )
  }

  showWeatherParis(){
    this.climaService.getWeatherParis().subscribe(
      data =>{
        this.temperaturaParis = Math.round(data['main']['temp']);
        this.humedadParis = data['main']['humidity'];
        this.vientoParis = data['wind']['speed'];
        if(data['weather']['main'] == 'Clouds'){
          this.iconoParis = 'nubes.png';
        }
        else if(data['weather']['main'] == 'Rain'){
          this.iconoParis = 'rain.png';
        }
        else{
          this.iconoParis = 'dom.png';
        }
      }
    )
  }

  showWeatherPredictionsBogota(){
    this.climaService.getWeatherPredictionsBogota().subscribe(
      data =>{
        this.hoyMinima = Math.round(data['list'][0]['main']['temp_min']);
        this.hoyMaxima = Math.round(data['list'][0]['main']['temp_max']);
        this.hoyTiempo = data['list'][0]['weather'][0]['description'];


        if(data['list'][0]['weather'][0]['main'] == 'Clouds'){
          this.hoyIcono = 'nubes.png';
        }
        else if(data['list'][0]['weather'][0]['main'] == 'Rain'){
          this.hoyIcono = 'rain.png';
        }
        else{
          this.hoyIcono = 'dom.png';
        }

        this.manianaMinima = Math.round(data['list'][1]['main']['temp_min']);
        this.manianaMaxima = Math.round(data['list'][1]['main']['temp_max']);
        this.manianaTiempo = data['list'][1]['weather'][0]['description'];


        if(data['list'][1]['weather'][0]['main'] == 'Clouds'){
          this.manianaIcono = 'nubes.png';
        }
        else if(data['list'][1]['weather'][0]['main'] == 'Rain'){
          this.manianaIcono = 'rain.png';
        }
        else{
          this.manianaIcono = 'dom.png';
        }

        this.pasadoMinima = Math.round(data['list'][2]['main']['temp_min']);
        this.pasadoMaxima = Math.round(data['list'][2]['main']['temp_max']);
        this.pasadoTiempo = data['list'][2]['weather'][0]['description'];


        if(data['list'][2]['weather'][0]['main'] == 'Clouds'){
          this.pasadoIcono = 'nubes.png';
        }
        else if(data['list'][2]['weather'][0]['main'] == 'Rain'){
          this.pasadoIcono = 'rain.png';
        }
        else{
          this.pasadoIcono = 'dom.png';
        }


      }
    )
  }
}
