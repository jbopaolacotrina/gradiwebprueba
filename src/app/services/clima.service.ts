import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ClimaService {
  datosApi: any;

  constructor(private http: HttpClient) {}

  getWeatherBogota(): Observable <any> {
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    return this.http.get( 'https://api.openweathermap.org/data/2.5/weather?q=Bogota&lang=es&appid=be66c3f5945f8b9500e762ba7db2ba50&units=metric');
  }

  getWeatherPredictionsBogota(): Observable <any> {
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    return this.http.get( 'https://api.openweathermap.org/data/2.5/forecast?q=Bogota&lang=es&appid=be66c3f5945f8b9500e762ba7db2ba50&units=metric&cnt=3');
  }

  getWeatherLyon(): Observable <any> {
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    return this.http.get( 'https://api.openweathermap.org/data/2.5/weather?q=Lyon&lang=es&appid=be66c3f5945f8b9500e762ba7db2ba50&units=metric');
  }

  getWeatherParis(): Observable <any> {
    const headers = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };

    return this.http.get( 'https://api.openweathermap.org/data/2.5/weather?q=Paris&lang=es&appid=be66c3f5945f8b9500e762ba7db2ba50&units=metric');
  }
}
